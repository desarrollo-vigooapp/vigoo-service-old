package com.vigoo.vigoobox.vigoobox;

import com.vigoo.vigoobox.vigoobox.datasource.NewDatasoruce;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static com.google.common.collect.Lists.newArrayList;


import java.util.*;

@SpringBootApplication
@EnableScheduling
@EnableSwagger2
@EnableTransactionManagement
@Import(BeanValidatorPluginsConfiguration.class)
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class VigooboxApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(VigooboxApplication.class, args);
    }

    @Bean
    public Docket swaggerApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors
                        .basePackage("com.vigoo.vigoobox.vigoobox.restservice"))
                .paths(PathSelectors.regex("/rest/.*"))
                .build()
                .pathMapping("/")
                .genericModelSubstitutes(ResponseEntity.class)
                .apiInfo(apiInfo())
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Collections.singletonList(securityContext()))
                .useDefaultResponseMessages(false)
                .globalResponseMessage(RequestMethod.POST,
                        newArrayList(new ResponseMessageBuilder()
                                        .code(500)
                                        .message("500 message")
                                        .responseModel(new ModelRef("Error"))
                                        .build(),
                                new ResponseMessageBuilder()
                                        .code(403)
                                        .message("Forbidden!")
                                        .build()));
    }


    private List<SecurityReference> defaultAuth() {
        final AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        final AuthorizationScope[] authorizationScopes = new AuthorizationScope[]{authorizationScope};
        return Collections.singletonList(new SecurityReference("Bearer", authorizationScopes));
    }


    private SecurityContext securityContext() {
        return SecurityContext.builder().securityReferences(defaultAuth()).forPaths(PathSelectors.regex("/.*")).build();
    }

    private ApiKey apiKey() {
        return new ApiKey("Bearer", "Authorization", "header");
    }


    private ApiInfo apiInfo() {
        Collection<VendorExtension> vendorExtensions = new ArrayList<>();
        return new ApiInfo("API REST Vigoo Box",
                "Servicios REST del Vigoo Box",
                "0.0.1-SNAPSHOT",
                "urn:tos",
                new Contact("Carlos Torres", "http://www.vigooapp.com", "desarrollo@vigooapp.com"),
                "API License",
                "http://www.api-license-url.com", vendorExtensions);
    }

    @Override
    @Transactional
    public void run(String... strings) {
        NewDatasoruce.create("admin");
    }

/*
      use buscame;
create table if not exists oauth_client_details (
  client_id VARCHAR(255) PRIMARY KEY,
  resource_ids VARCHAR(255),
  client_secret VARCHAR(255),
  scope VARCHAR(255),
  authorized_grant_types VARCHAR(255),
  web_server_redirect_uri VARCHAR(255),
  authorities VARCHAR(255),
  access_token_validity INTEGER,
  refresh_token_validity INTEGER,
  additional_information VARCHAR(4096),
  autoapprove VARCHAR(255)
);


      INSERT INTO `oauth_client_details` (`client_id`, `client_secret`, `scope`, `authorized_grant_types`, `access_token_validity`, `additional_information`)
        VALUES
                ('vigooApp', '$2a$10$oH0eIK3wREcD/c1koaxEV.zlSZ85UFL/jC3oCV/aR2XMD4o7Ax406', 'read,write,trust', 'authorization_code,password,refresh_token,implicit', '900', '{}')

      dmlnb29BcHA6Q2FyamVzMS4=
      */


}


