package com.vigoo.vigoobox.vigoobox.dao;

import java.io.Serializable;

public class PestanaDao implements Serializable {

    private String name;
    private String id;
    private CampoDao[] campoDaos;

    public PestanaDao(String name, CampoDao[] campoDaos) {
        this.id = name.trim().replaceAll(" ","_");
        this.name = name;
        this.campoDaos = campoDaos;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CampoDao[] getCampoDaos() {
        return campoDaos;
    }

    public void setCampoDaos(CampoDao[] campoDaos) {
        this.campoDaos = campoDaos;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
