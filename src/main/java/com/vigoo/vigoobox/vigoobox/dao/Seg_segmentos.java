package com.vigoo.vigoobox.vigoobox.dao;




public class Seg_segmentos {

    private String codigo;
    private String descripcion;

    public Seg_segmentos() {
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
