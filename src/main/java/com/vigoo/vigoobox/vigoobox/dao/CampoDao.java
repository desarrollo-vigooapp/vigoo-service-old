package com.vigoo.vigoobox.vigoobox.dao;

import java.io.Serializable;

public class CampoDao implements Serializable {

    private String name;
    private String tipo;
    private String tamano;
    private boolean requerido;
    private String selectCombo;
    private String valor;
    private SelectDao[] selectDaos;
    private String nombrePagina;

    public CampoDao() {
    }

    public CampoDao(String name, String type, String tamano, String requerido, String selectCombo, String value) {
        this.name = name;
        switch (type) {
            case "varchar":
                this.tipo = "text";
                break;
            case "numeric":
                this.tipo = "number";
                break;
            case "decimal":
                this.tipo = "decimal";
                break;
            case "combo":
                this.tipo = "select";
                break;
            case "date":
                this.tipo = "date";
                break;
        }
        this.tamano = tamano;
        if (requerido.equals("1")) {
            this.requerido = true;
        } else {
            this.requerido = false;
        }
        if(selectCombo==null){
            this.selectCombo = "";
        }else{
            this.selectCombo = selectCombo;
        }

            this.valor=value;


    }

    public CampoDao(String name, String type, String tamano, String requerido,String selectCombo) {
        this.name = name;
        switch (type) {
            case "varchar":
                this.tipo = "text";
                break;
            case "numeric":
                this.tipo = "number";
                break;
            case "decimal":
                this.tipo = "decimal";
                break;
            case "combo":
                this.tipo = "select";
                break;
            case "date":
                this.tipo = "date";
                break;
        }
        this.tamano = tamano;
        if (requerido.equals("1")) {
            this.requerido = true;
        } else {
            this.requerido = false;
        }
        if(selectCombo==null){
            this.selectCombo = "";
        }else{
            this.selectCombo = selectCombo;
        }

    }

    public CampoDao(String name, String tipo, String tamano, String requerido, String selectCombo, String valor,String nombrePagina) {
        this.name = name;
        switch (tipo) {
            case "varchar":
                this.tipo = "text";
                break;
            case "numeric":
                this.tipo = "number";
                break;
            case "decimal":
                this.tipo = "decimal";
                break;
            case "combo":
                this.tipo = "select";
                break;
            case "date":
                this.tipo = "date";
                break;
        }
        this.tamano = tamano;
        if (requerido.equals("1")) {
            this.requerido = true;
        } else {
            this.requerido = false;
        }
        this.selectCombo = selectCombo;
        this.valor = valor;
        this.nombrePagina = nombrePagina;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String type) {
        this.tipo = type;
    }

    public String getTamano() {
        return tamano;
    }

    public void setTamano(String tamano) {
        this.tamano = tamano;
    }

    public boolean isRequerido() {
        return requerido;
    }

    public void setRequerido(boolean requerido) {
        this.requerido = requerido;
    }

    public String getSelectCombo() {
        return selectCombo;
    }

    public void setSelectCombo(String selectCombo) {
        this.selectCombo = selectCombo;
    }

    public String getValor() {

        if(valor==null){
            valor="";
        }
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public SelectDao[] getSelectDaos() {
        return selectDaos;
    }

    public void setSelectDaos(SelectDao[] selectDaos) {
        this.selectDaos = selectDaos;
    }

    public String getNombrePagina() {
        return nombrePagina;
    }

    public void setNombrePagina(String nombrePagina) {
        this.nombrePagina = nombrePagina;
    }
}
