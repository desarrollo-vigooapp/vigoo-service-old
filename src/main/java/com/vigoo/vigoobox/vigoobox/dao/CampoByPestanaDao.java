package com.vigoo.vigoobox.vigoobox.dao;

public class CampoByPestanaDao {

    private PestanaDao[] pestanaDaos;

    public CampoByPestanaDao(PestanaDao[] pestanaDaos) {
        this.pestanaDaos = pestanaDaos;
    }

    public PestanaDao[] getPestanaDaos() {
        return pestanaDaos;
    }

    public void setPestanaDaos(PestanaDao[] pestanaDaos) {
        this.pestanaDaos = pestanaDaos;
    }
}
