package com.vigoo.vigoobox.vigoobox.dao;

import java.io.Serializable;

public class SolicitudReajusteNominaDao implements Serializable {

    private CamposForSaveDAO camposForSaveDAO;
    private CamposForSaveDAO camposForSaveDAO2;
    private ResultFindAllGenericDao resultTable;

    public SolicitudReajusteNominaDao() {
    }

    public SolicitudReajusteNominaDao(CamposForSaveDAO camposForSaveDAO, ResultFindAllGenericDao resultTable) {
        this.camposForSaveDAO = camposForSaveDAO;
        this.resultTable = resultTable;
    }

    public SolicitudReajusteNominaDao(CamposForSaveDAO camposForSaveDAO, CamposForSaveDAO camposForSaveDAO2, ResultFindAllGenericDao resultTable) {
        this.camposForSaveDAO = camposForSaveDAO;
        this.camposForSaveDAO2 = camposForSaveDAO2;
        this.resultTable = resultTable;
    }

    public CamposForSaveDAO getCamposForSaveDAO() {
        return camposForSaveDAO;
    }

    public void setCamposForSaveDAO(CamposForSaveDAO camposForSaveDAO) {
        this.camposForSaveDAO = camposForSaveDAO;
    }

    public CamposForSaveDAO getCamposForSaveDAO2() {
        return camposForSaveDAO2;
    }

    public void setCamposForSaveDAO2(CamposForSaveDAO camposForSaveDAO2) {
        this.camposForSaveDAO2 = camposForSaveDAO2;
    }

    public ResultFindAllGenericDao getResultTable() {
        return resultTable;
    }

    public void setResultTable(ResultFindAllGenericDao resultTable) {
        this.resultTable = resultTable;
    }
}
