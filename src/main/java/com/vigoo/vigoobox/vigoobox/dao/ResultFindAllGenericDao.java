package com.vigoo.vigoobox.vigoobox.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ResultFindAllGenericDao implements Serializable {
    private Integer totalRegisters;
    private String[] headers;
    private List<Object[]> data;

    public ResultFindAllGenericDao() {
    }

    public Integer getTotalRegisters() {
        return totalRegisters;
    }

    public void setTotalRegisters(Integer totalRegisters) {
        this.totalRegisters = totalRegisters;
    }
    public String[] getHeaders() {
        return headers;
    }

    public void setHeaders(String[] headers) {
        this.headers = headers;
    }

    public List<Object[]> getData() {
        if(data == null){
            data = new ArrayList<>();
        }
        return data;
    }

    public void setData(List<Object[]> data) {
        this.data = data;
    }
}
