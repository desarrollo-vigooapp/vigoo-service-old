package com.vigoo.vigoobox.vigoobox.dao;

import java.io.Serializable;

public class CamposForSaveDAO implements Serializable {

    private CampoDao[] campoDaos;
    private Object[] values;

    public CamposForSaveDAO() {
    }

    public CamposForSaveDAO(CampoDao[] campoDaos) {
        this.campoDaos = campoDaos;
    }

    public CampoDao[] getCampoDaos() {
        return campoDaos;
    }

    public void setCampoDaos(CampoDao[] campoDaos) {
        this.campoDaos = campoDaos;
    }

    public Object[] getValues() {
        if(values==null)
            values = new String[60];
        return values;
    }

    public void setValues(Object[] values) {
        this.values = values;
    }
}
