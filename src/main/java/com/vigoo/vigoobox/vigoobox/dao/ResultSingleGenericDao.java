package com.vigoo.vigoobox.vigoobox.dao;

import java.util.HashMap;
import java.util.Map;

public class ResultSingleGenericDao {

    private Map<String, String> data;

    public ResultSingleGenericDao() {
    }

    public Map<String, String> getData() {
        if (data == null) {
            data = new HashMap<>();
        }
        return data;
    }
}
