package com.vigoo.vigoobox.vigoobox.dao;

import java.io.Serializable;

public class SelectDao implements Serializable {

    private Object valor;
    private String texto;

    public SelectDao(Object valor, String texto) {
        this.valor = valor;
        this.texto = texto;
    }

    public Object getValor() {
        return valor;
    }

    public void setValor(Object valor) {
        this.valor = valor;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}
