package com.vigoo.vigoobox.vigoobox.services;

import com.vigoo.vigoobox.vigoobox.constants.SpConstant;
import com.vigoo.vigoobox.vigoobox.dao.ResultSingleGenericDao;
import com.vigoo.vigoobox.vigoobox.entities.Usuario;
import com.vigoo.vigoobox.vigoobox.jdbctemplate.JdbcSingleTemplateSP;
import com.vigoo.vigoobox.vigoobox.repository.UsuarioRepository;
import com.vigoo.vigoobox.vigoobox.util.LoadParameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;


@Service("userDetailService")
@Primary
public class UserDetailServiceImp implements UserDetailsService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserBuilder userBuilder = null;
        Usuario usuario = usuarioRepository.findByCorreo(username);
        if (usuario!= null) {
            Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
            authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
            userBuilder = org.springframework.security.core.userdetails.User.withUsername(username);
            userBuilder.disabled(false);
            userBuilder.password(usuario.getContrasena());
            userBuilder.authorities(authorities);
        } else {
            throw new UsernameNotFoundException("users not found");
        }
        return userBuilder.build();
    }
}
