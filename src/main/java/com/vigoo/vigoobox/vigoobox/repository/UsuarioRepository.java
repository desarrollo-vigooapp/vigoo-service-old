package com.vigoo.vigoobox.vigoobox.repository;

import com.vigoo.vigoobox.vigoobox.entities.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

    Usuario findByCorreo(String correo);
}
