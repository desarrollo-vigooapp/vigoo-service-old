package com.vigoo.vigoobox.vigoobox.jdbctemplate;

import com.vigoo.vigoobox.vigoobox.dao.ResultFindAllGenericDao;
import com.vigoo.vigoobox.vigoobox.singleton.DatasourceSingleton;
import org.springframework.jdbc.core.JdbcTemplate;

public class JdbcFindAllFieldsTemplateSP {

    public static ResultFindAllGenericDao run(String bd, String sp, Object[] parameters) {
        ResultFindAllGenericDao result = new ResultFindAllGenericDao();
        JdbcTemplate jdbcTemplate = DatasourceSingleton.getJdbcTemplateMap().get(bd);
        jdbcTemplate.query(sp, parameters, rs -> {
            int max = rs.getMetaData().getColumnCount();
            String[] header = new String[max];
            Object[] data = new Object[max];
            for (int i = 1; i <= max; i++) {
                if (header.length <= max) {
                    header[i - 1] = rs.getMetaData().getColumnName(i);
                }
                data[i - 1] = rs.getString(rs.getMetaData().getColumnName(i));

            }
            result.setHeaders(header);
            result.getData().add(data);
        });
        return result;
    }
}
