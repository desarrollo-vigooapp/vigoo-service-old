package com.vigoo.vigoobox.vigoobox.jdbctemplate;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import javax.sql.DataSource;
import java.sql.SQLException;

public class NewJdbcTemplate {

    public static JdbcTemplate create(DataSource dataSource) {
        try {
            return new JdbcTemplate(new SingleConnectionDataSource(dataSource.getConnection(), true));
        } catch (SQLException e) {
            return new JdbcTemplate(dataSource);
        }
    }
}
