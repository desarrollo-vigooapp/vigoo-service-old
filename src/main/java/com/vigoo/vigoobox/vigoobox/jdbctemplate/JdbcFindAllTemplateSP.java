package com.vigoo.vigoobox.vigoobox.jdbctemplate;

import com.vigoo.vigoobox.vigoobox.dao.ResultFindAllGenericDao;
import com.vigoo.vigoobox.vigoobox.singleton.DatasourceSingleton;
import org.springframework.jdbc.core.JdbcTemplate;

public class JdbcFindAllTemplateSP {

    public static ResultFindAllGenericDao run(String bd, String sp, Object[] parameters) {
        ResultFindAllGenericDao result = new ResultFindAllGenericDao();
        JdbcTemplate jdbcTemplate = DatasourceSingleton.getJdbcTemplateMap().get(bd);
        jdbcTemplate.query(sp, parameters, rs -> {
            int max = rs.getMetaData().getColumnCount();
            String[] header = new String[max - 1];
            Object[] data = new Object[max - 1];
            for (int i = 1; i <= max; i++) {
                if (i > 1) {
                    if (header.length <= max - 1) {
                        header[i - 2] = rs.getMetaData().getColumnName(i);
                    }
                    data[i - 2] = rs.getString(rs.getMetaData().getColumnName(i));
                } else {
                    result.setTotalRegisters(Integer.valueOf(rs.getString(rs.getMetaData().getColumnName(i))));
                }
            }
            result.setHeaders(header);
            result.getData().add(data);
        });
        return result;
    }
}
