package com.vigoo.vigoobox.vigoobox.jdbctemplate;

import com.vigoo.vigoobox.vigoobox.dao.ResultSingleGenericDao;
import com.vigoo.vigoobox.vigoobox.singleton.DatasourceSingleton;
import org.springframework.jdbc.core.JdbcTemplate;

public class JdbcSingleTemplateSP {

    public static ResultSingleGenericDao run(String bd, String sp, Object[] parameters) {
        ResultSingleGenericDao result = new ResultSingleGenericDao();
        JdbcTemplate jdbcTemplate = DatasourceSingleton.getJdbcTemplateMap().get(bd);
        try {
            if (jdbcTemplate != null) {
                jdbcTemplate.query(sp, parameters, rs -> {
                    int max = rs.getMetaData().getColumnCount();
                    for (int i = 1; i <= max; i++) {
                        result.getData().put(rs.getMetaData().getColumnName(i), rs.getString(rs.getMetaData().getColumnName(i)));
                    }
                });
            }
        }catch (Exception e){
            System.out.println("Sql Error:");
            System.out.println(e.getMessage());
            System.out.println("Sql causa");
            System.out.println(e.getCause());

        }
        return result;
    }
}
