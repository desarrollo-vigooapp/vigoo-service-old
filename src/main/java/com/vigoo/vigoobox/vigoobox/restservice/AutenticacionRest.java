package com.vigoo.vigoobox.vigoobox.restservice;

import com.vigoo.vigoobox.vigoobox.dao.ResponseData;
import com.vigoo.vigoobox.vigoobox.util.LoadParameters;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/rest/authentication")
public class AutenticacionRest {

    @Autowired
    private TokenStore tokenStore;


    @ApiOperation(value = "Iniciar sesion", notes = "Servicio para inciar sesion")
    @PostMapping(value = "/login",
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_JSON_VALUE})
    private ResponseEntity<ResponseData> inicioDeSesion(@RequestParam Map<String, String> body) {
        Object[] parameters = LoadParameters.run(new Object[]{"usuario", "acceso", body.get("username"), body.get("password")});
        Map<String,String> response = new HashMap<>();
        response.put("nombre","carlos");
        response.put("box","Alpha");
        response.put("meta_diaria","1");
        return ResponseEntity.ok(new ResponseData(200,"Ok",response));
    }

    @ApiOperation(value = "Crear cuenta", notes = "Servicio para crear cuenta")
    @PostMapping(value = "/crear_cuenta",
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_JSON_VALUE})
    private ResponseEntity<ResponseData> crearCuenta(@RequestParam Map<String, String> body) {

        Map<String,String> response = new HashMap<>();
        response.put("nombre","carlos");
        response.put("box","Alpha");
        response.put("meta_diaria","1");
        return ResponseEntity.ok(new ResponseData(200,"Ok",response));
    }

    @ApiOperation(value = "Cambiar contraseña", notes = "Servicio para cambiar contraseña")
    @PostMapping(value = "/cambiar_contrasena",
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_JSON_VALUE})
    private ResponseEntity<ResponseData> cambiarContrasena(@RequestParam Map<String, String> body) {

        Map<String,String> response = new HashMap<>();
        response.put("nombre","carlos");
        response.put("box","Alpha");
        response.put("meta_diaria","1");
        return ResponseEntity.ok(new ResponseData(200,"Ok",response));
    }

    @RequestMapping(value = "/revoke-token", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("permitAll()")
    public ResponseEntity<ResponseData> logout(HttpServletRequest request) {
        String authHeader = request.getHeader("Authorization");
        if (authHeader != null) {
            String tokenValue = authHeader.replace("Bearer", "").trim();
            OAuth2AccessToken accessToken = tokenStore.readAccessToken(tokenValue);
            tokenStore.removeAccessToken(accessToken);
        }
        return ResponseEntity.ok(new ResponseData(200,"Ok",null));
    }
}