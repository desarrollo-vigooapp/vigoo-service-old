package com.vigoo.vigoobox.vigoobox.enums;

public enum EstadoBox {

    ACTIVO,
    INACTIVO;

    EstadoBox() {
    }
}
