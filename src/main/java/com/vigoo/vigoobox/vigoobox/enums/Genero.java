package com.vigoo.vigoobox.vigoobox.enums;

public enum Genero {

    MASCULINO,
    FEMENINO;

    Genero() {
    }
}
