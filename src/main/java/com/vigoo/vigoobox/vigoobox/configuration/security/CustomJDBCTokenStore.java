package com.vigoo.vigoobox.vigoobox.configuration.security;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

import javax.sql.DataSource;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;

public class CustomJDBCTokenStore extends JdbcTokenStore {

    public CustomJDBCTokenStore(DataSource dataSource) {
        super(dataSource);
    }




    @Override
    protected OAuth2AccessToken deserializeAccessToken(byte[] token) {
        return super.deserializeAccessToken(token);
    }

    @Override
    protected String extractTokenKey(String value) {
        if (value == null) {
            return null;
        }
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
        }
        catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("MD5 algorithm not available.  Fatal (should be in the JDK).");
        }

        byte[] bytes = digest.digest(value.getBytes(StandardCharsets.UTF_8));
        return String.format("%032x", new BigInteger(1, bytes));


    }


    @Override
    public OAuth2AccessToken readAccessToken(String tokenValue) {
        OAuth2AccessToken accessToken = null;

        try {
            accessToken = new DefaultOAuth2AccessToken(tokenValue);
        }
        catch (EmptyResultDataAccessException e) {

        }
        catch (IllegalArgumentException e) {
            removeAccessToken(tokenValue);
        }

        return accessToken;
    }

    @Override
    public Collection<OAuth2AccessToken> findTokensByUserName(String userName) {
        return super.findTokensByUserName(userName);
    }
}
