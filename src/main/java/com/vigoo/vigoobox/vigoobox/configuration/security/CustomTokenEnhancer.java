package com.vigoo.vigoobox.vigoobox.configuration.security;

import com.vigoo.vigoobox.vigoobox.constants.SpConstant;
import com.vigoo.vigoobox.vigoobox.dao.ResultFindAllGenericDao;
import com.vigoo.vigoobox.vigoobox.entities.Usuario;
import com.vigoo.vigoobox.vigoobox.jdbctemplate.JdbcFindAllTemplateSP;
import com.vigoo.vigoobox.vigoobox.repository.UsuarioRepository;
import com.vigoo.vigoobox.vigoobox.services.UserDetailServiceImp;
import com.vigoo.vigoobox.vigoobox.util.LoadParameters;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;


import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CustomTokenEnhancer implements TokenEnhancer {

    private UserDetailServiceImp customUserDetailsService;
    private UsuarioRepository usuarioRepository;

    @Autowired
    public CustomTokenEnhancer(UserDetailServiceImp customUserDetailsService, UsuarioRepository usuarioRepository) {
        this.customUserDetailsService = customUserDetailsService;
        this.usuarioRepository = usuarioRepository;
    }

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        User user = (User) authentication.getPrincipal();
        Usuario usuario = usuarioRepository.findByCorreo(user.getUsername());
        Object[] parameters = LoadParameters.run(new Object[]{"box", "consulta", usuario.getCorreo()});
        ResultFindAllGenericDao result = JdbcFindAllTemplateSP.run("admin", SpConstant.SP_ACCESO, parameters);
        JSONArray jsonA = new JSONArray();
        for(int i=0;i<result.getTotalRegisters();i++){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id",result.getData().get(i)[0]);
            jsonObject.put("nombre",result.getData().get(i)[1]);
            jsonA.put(jsonObject);
        }
        final Map<String, Object> additionalInfo = new HashMap<>();
        additionalInfo.put("authorities", user.getAuthorities());
        additionalInfo.put("usuario", usuario);
        additionalInfo.put("boxes",jsonA.toList());
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
        return accessToken;
    }

}