package com.vigoo.vigoobox.vigoobox.singleton;

import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

public class DatasourceSingleton {

    private static DatasourceSingleton ourInstance = new DatasourceSingleton();
    private static Map<String, DataSource> dataSourceMap = new HashMap<>();
    private static Map<String, JdbcTemplate> jdbcTemplateMap = new HashMap<>();

    private DatasourceSingleton() {
    }

    public static DatasourceSingleton getInstance() {
        return ourInstance;
    }

    public static Map<String, DataSource> getDataSourceMap() {
        return dataSourceMap;
    }

    public static Map<String, JdbcTemplate> getJdbcTemplateMap() {
        return jdbcTemplateMap;
    }

    public static void addDataSource(String bd, DataSource dataSource) {
        getDataSourceMap().put(bd, dataSource);
    }

    public static void addJdbcTemplate(String bd, JdbcTemplate jdbcTemplate) {
        getJdbcTemplateMap().put(bd, jdbcTemplate);
    }

}
