package com.vigoo.vigoobox.vigoobox.datasource;

import org.apache.tomcat.jdbc.pool.DataSource;

public class DataSourceBean {

    private String bd;
    private DataSource dataSource;

    public void DataSourceBean(String bd) {
        this.bd = bd;
        org.apache.tomcat.jdbc.pool.DataSource dataSource = new org.apache.tomcat.jdbc.pool.DataSource();  // org.apache.tomcat.jdbc.pool.DataSource;
        dataSource.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        dataSource.setUrl("jdbc:sqlserver://104.154.159.219;databaseName=vigoo-admin");
        //dataSource.setUrl("jdbc:sqlserver://10.128.0.2;databaseName=vigoo-admin");
        dataSource.setUsername("sa");
        dataSource.setPassword("Carjes1.");
        dataSource.setTestWhileIdle(true);
        dataSource.setTimeBetweenEvictionRunsMillis(30000);
        dataSource.setValidationQuery("SELECT 1");
        this.dataSource  = dataSource;
    }

    public String getBd() {
        return bd;
    }

    public void setBd(String bd) {
        this.bd = bd;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
