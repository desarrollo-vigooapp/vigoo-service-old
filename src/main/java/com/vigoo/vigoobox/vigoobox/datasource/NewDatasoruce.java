package com.vigoo.vigoobox.vigoobox.datasource;

import com.vigoo.vigoobox.vigoobox.jdbctemplate.NewJdbcTemplate;
import com.vigoo.vigoobox.vigoobox.singleton.DatasourceSingleton;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;

public class NewDatasoruce {

    public static String create(String bd) {
        DataSource dataSource = new DataSource();  // org.apache.tomcat.jdbc.pool.DataSource;
        dataSource.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        dataSource.setUrl("jdbc:sqlserver://104.154.159.219;databaseName=vigoo-admin");
        //dataSource.setUrl("jdbc:sqlserver://10.128.0.2;databaseName=vigoo-admin");
        dataSource.setUsername("sa");
        dataSource.setPassword("Carjes1.");
        dataSource.setTestWhileIdle(true);
        dataSource.setTimeBetweenEvictionRunsMillis(30000);
        dataSource.setValidationQuery("SELECT 1");
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        BeanDefinitionBuilder b = BeanDefinitionBuilder.rootBeanDefinition(DataSourceBean.class)
                .addPropertyValue("bd", bd)
                .addPropertyValue("dataSource", dataSource);
        beanFactory.registerBeanDefinition(bd, b.getBeanDefinition());
        DatasourceSingleton.addDataSource(bd, dataSource);
        DatasourceSingleton.addJdbcTemplate(bd, NewJdbcTemplate.create(dataSource));
        return "ok";

    }


}
