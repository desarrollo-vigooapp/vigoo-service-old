package com.vigoo.vigoobox.vigoobox.util;


import com.vigoo.vigoobox.vigoobox.jdbctemplate.JdbcSingleTemplateSP;

import java.util.Map;

public class ValidateAccess {

    public static Map<String, String> run(String db, String sp, Object[] parameters) {
        return JdbcSingleTemplateSP.run(db, sp, parameters).getData();
    }
}
