package com.vigoo.vigoobox.vigoobox.util;




import com.vigoo.vigoobox.vigoobox.dao.CampoByPestanaDao;
import com.vigoo.vigoobox.vigoobox.dao.CampoDao;
import com.vigoo.vigoobox.vigoobox.dao.PestanaDao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DeterminePagination implements Serializable {

    public static CampoByPestanaDao evaluate(CampoDao[] campoDaos) {
        if (campoDaos[0].getNombrePagina() == null || ((String) campoDaos[0].getNombrePagina()).equals("")) {
            return null;
        }
        String[] tabs = new String[campoDaos.length];
        int count = 0;
        Map<String, List<CampoDao>> mapCampoByPestanaDao = new HashMap<>();
        for (CampoDao campoDao : campoDaos) {
            if (mapCampoByPestanaDao.containsKey(campoDao.getNombrePagina())) {
                mapCampoByPestanaDao.get(campoDao.getNombrePagina()).add(campoDao);
            } else {
                tabs[count] = campoDao.getNombrePagina();
                List<CampoDao> campoDaosAux = new ArrayList<>();
                campoDaosAux.add(campoDao);
                mapCampoByPestanaDao.put(campoDao.getNombrePagina(), campoDaosAux);
                count++;
            }
        }
        List<PestanaDao> pestanaDaos = new ArrayList<>();
        for (String key : tabs) {
            if (key == null) {
                break;
            }
            CampoDao[] campoDaosAux2 = new CampoDao[mapCampoByPestanaDao.get(key).size()];
            PestanaDao pestanaDao = new PestanaDao(key, mapCampoByPestanaDao.get(key).toArray(campoDaosAux2));
            pestanaDaos.add(pestanaDao);
        }
        PestanaDao[] pestanaDaosAux = new PestanaDao[pestanaDaos.size()];
        CampoByPestanaDao campoByPestanaDaos = new CampoByPestanaDao(pestanaDaos.toArray(pestanaDaosAux));
        return campoByPestanaDaos;
    }
}
