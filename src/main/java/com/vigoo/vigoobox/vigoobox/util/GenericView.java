package com.vigoo.vigoobox.vigoobox.util;

import com.vigoo.vigoobox.vigoobox.dao.*;
import com.vigoo.vigoobox.vigoobox.jdbctemplate.JdbcFindAllFieldsTemplateSP;
import com.vigoo.vigoobox.vigoobox.jdbctemplate.JdbcFindAllTemplateQuery;
import com.vigoo.vigoobox.vigoobox.jdbctemplate.JdbcFindAllTemplateSP;
import com.vigoo.vigoobox.vigoobox.jdbctemplate.JdbcSingleTemplateSP;
import org.springframework.ui.Model;

import javax.servlet.http.HttpSession;
import java.util.Map;

public class GenericView {

    public static String cargarGrilla(HttpSession session, Model model, String sp, String forma, String operacion, String accionEditar, String accionEliminar, String accionAgregar, String accionVer, String acionBuscar) {
        String db = session.getAttribute("empresa").toString();
        String usuario = session.getAttribute("usuario").toString();
        Object[] parametersAccess = LoadParameters.run(new Object[]{forma, "acceso", usuario});
        Map<String, String> access = ValidateAccess.run(db, sp, parametersAccess);
        if (access.containsKey("Error")) {
            model.addAttribute("error", access.get("Error"));
            return "acceso-restringido";
        } else {
            Object[] parameters = LoadParameters.run(new Object[]{forma, operacion, usuario, 1});
            ResultFindAllGenericDao result = JdbcFindAllTemplateSP.run(db, sp, parameters);
            model.addAttribute("resultado", (String) model.asMap().get("resultado"));
            model.addAttribute("result", result);
            model.addAttribute("mensaje", (String) model.asMap().get("mensaje"));
            model.addAttribute("totalRegisters", result.getTotalRegisters());
            model.addAttribute("acionBuscar", acionBuscar);
            model.addAttribute("accionEditar", accionEditar);
            model.addAttribute("accionEliminar", accionEliminar);
            model.addAttribute("accionAgregar", accionAgregar);
            model.addAttribute("add", access.get("Adicionar"));
            model.addAttribute("delete", access.get("Borrar"));
            model.addAttribute("accionVer", accionVer);
            model.addAttribute("update", access.get("Modificar"));
            model.addAttribute("titulo", access.get("Titulo"));


            model.addAttribute("totalPaginas",Math.ceil(result.getTotalRegisters()/50.0));
            model.addAttribute("paginaActual",1);
            model.addAttribute("paginaAtras",0);

            return "general/grilla";
        }
    }

    public static String cargarGrillaPage(HttpSession session, Model model, String sp, String forma, String operacion, String accionEditar, String accionEliminar, String accionAgregar, String accionVer, int page) {
        String db = session.getAttribute("empresa").toString();
        String usuario = session.getAttribute("usuario").toString();
        Object[] parametersAccess = LoadParameters.run(new Object[]{forma, "acceso", usuario});
        Map<String, String> access = ValidateAccess.run(db, sp, parametersAccess);
        if (access.containsKey("Error")) {
            model.addAttribute("error", access.get("Error"));
            return "acceso-restringido";
        } else {
            Object[] parameters = LoadParameters.run(new Object[]{forma, operacion, usuario, page});
            ResultFindAllGenericDao result = JdbcFindAllTemplateSP.run(db, sp, parameters);
            model.addAttribute("result", result);
            model.addAttribute("resultado", (String) model.asMap().get("resultado"));
            model.addAttribute("mensaje", (String) model.asMap().get("mensaje"));
            model.addAttribute("totalRegisters", result.getTotalRegisters());
            model.addAttribute("add", access.get("Adicionar"));
            model.addAttribute("delete", access.get("Borrar"));
            model.addAttribute("update", access.get("Modificar"));
            model.addAttribute("titulo", access.get("Titulo"));
            model.addAttribute("accionEditar", accionEditar);
            model.addAttribute("accionVer", accionVer);
            model.addAttribute("accionEliminar", accionEliminar);
            model.addAttribute("accionAgregar", accionAgregar);
            model.addAttribute("totalPaginas",Math.ceil(result.getTotalRegisters()/50.0));
            model.addAttribute("paginaActual",page+1);
            model.addAttribute("paginaAtras",page-1);
            return "general/grilla::tablaGenerica";
        }
    }

    public static String cargarGrillaBuscar(HttpSession session, Model model, String sp, String forma, String operacion, String accionEditar, String accionEliminar, String accionAgregar, String accionVer, String buscar, int page) {
        String db = session.getAttribute("empresa").toString();
        String usuario = session.getAttribute("usuario").toString();
        Object[] parametersAccess = LoadParameters.run(new Object[]{forma, "acceso", usuario});
        Map<String, String> access = ValidateAccess.run(db, sp, parametersAccess);
        if (access.containsKey("Error")) {
            model.addAttribute("error", access.get("Error"));
            return "acceso-restringido";
        } else {
            Object[] parameters = LoadParameters.run(new Object[]{forma, operacion, usuario, 1,buscar,});
            ResultFindAllGenericDao result = JdbcFindAllTemplateSP.run(db, sp, parameters);
            model.addAttribute("result", result);
            model.addAttribute("resultado", (String) model.asMap().get("resultado"));
            model.addAttribute("mensaje", (String) model.asMap().get("mensaje"));
            model.addAttribute("totalRegisters", result.getTotalRegisters());
            model.addAttribute("add", access.get("Adicionar"));
            model.addAttribute("delete", access.get("Borrar"));
            model.addAttribute("update", access.get("Modificar"));
            model.addAttribute("titulo", access.get("Titulo"));
            model.addAttribute("accionEditar", accionEditar);
            model.addAttribute("accionVer", accionVer);
            model.addAttribute("accionEliminar", accionEliminar);
            model.addAttribute("accionAgregar", accionAgregar);
            model.addAttribute("totalPaginas",Math.ceil(result.getTotalRegisters()/50.0));
            model.addAttribute("paginaActual",page);
            model.addAttribute("paginaAtras",page-1);
            return "general/grilla::tablaGenerica";
        }
    }

    public static String cargarFormulario(HttpSession session, Model model, String sp, String forma, String operacion, String accion, String actionForm, CampoDao[] campos, String accionBoton, String acionVolver, String id, boolean soloLeer) {
        String db = session.getAttribute("empresa").toString();
        String usuario = session.getAttribute("usuario").toString();
        Object[] parametersAccess = LoadParameters.run(new Object[]{forma, "acceso", usuario});
        Map<String, String> access = ValidateAccess.run(db, sp, parametersAccess);
        if (access.containsKey("Error")) {
            model.addAttribute("error", access.get("Error"));
            return "acceso-restringido";
        } else {
            Object[] parameters = LoadParameters.run(new Object[]{forma, operacion, usuario});
            if (id != null) {
                parameters[3] = id;
            }
            ResultFindAllGenericDao result = JdbcFindAllFieldsTemplateSP.run(db, sp, parameters);
            CampoDao[] campoDaos = new CampoDao[result.getHeaders().length];
            for (int i = 0; i < result.getHeaders().length; i++) {
                if (campos != null) {
                    campoDaos[i] = new CampoDao((String) result.getHeaders()[i], (String) result.getData().get(0)[i], (String) result.getData().get(1)[i], (String) result.getData().get(2)[i], (String) result.getData().get(3)[i], campos[i].getValor());
                } else {
                    campoDaos[i] = new CampoDao((String) result.getHeaders()[i], (String) result.getData().get(0)[i], (String) result.getData().get(1)[i], (String) result.getData().get(2)[i], (String) result.getData().get(3)[i],(String) result.getData().get(4)[i] ,(String) result.getData().get(5)[i]);
                }
            }
            for (int i = 0; i < campoDaos.length; i++) {
                if (campoDaos[i].getTipo()!= null && campoDaos[i].getTipo().equals("select")) {
                    campoDaos[i].setValor(campoDaos[i].getValor().toUpperCase());
                    ResultFindAllGenericDao resultCombo = JdbcFindAllTemplateQuery.run(db, campoDaos[i].getSelectCombo());
                    SelectDao[] selectDaos = new SelectDao[resultCombo.getData().size()];
                    for (int j = 0; j < resultCombo.getData().size(); j++) {
                        selectDaos[j] = new SelectDao(resultCombo.getData().get(j)[0].toString().toUpperCase(), (String) resultCombo.getData().get(j)[1].toString().toUpperCase());
                    }
                    campoDaos[i].setSelectDaos(selectDaos);
                }
            }
            CampoByPestanaDao campoByPestanaDaos = DeterminePagination.evaluate(campoDaos);
            if(campoByPestanaDaos==null){
                model.addAttribute("pestanas", false);
            }else{
                model.addAttribute("pestanas", true);
            }
            model.addAttribute("campoByPestanaDaos", campoByPestanaDaos);
            model.addAttribute("camposForSaveDAO", new CamposForSaveDAO(campoDaos));
            model.addAttribute("add", access.get("Adicionar"));
            model.addAttribute("titulo", accion + " " + access.get("Titulo"));
            model.addAttribute("actionForm", actionForm);
            model.addAttribute("accionBoton", accionBoton);
            model.addAttribute("acionVolver", acionVolver);
            model.addAttribute("soloLeer", soloLeer);
            return "general/formulario";
        }
    }


    public static String[] grabarNuevo(HttpSession session, String sp, String forma, String operacion, CampoDao[] campoDaos, String accion) {
        String[] respuesta = new String[2];
        String db = session.getAttribute("empresa").toString();
        String usuario = session.getAttribute("usuario").toString();
        Object[] parameters = LoadParameters.run(new Object[]{forma, operacion, usuario});
        int j = 3;
        if (accion.equals("AGREGAR")) {
            for (int i = 0; i < campoDaos.length; i++) {
                parameters[j] = campoDaos[i].getValor();
                j++;
            }
        } else {
            for (int i = 0; i < campoDaos.length; i++) {
                if (i == 0) {
                    parameters[j] = campoDaos[i].getValor();
                    parameters[j + 1] = "";
                    j++;
                } else {
                    parameters[j] = campoDaos[i].getValor();
                }
                j++;
            }
        }
        ResultSingleGenericDao result = JdbcSingleTemplateSP.run(db, sp, parameters);
        if (result.getData().containsKey("Error")) {
            respuesta[0] = "Error";
            respuesta[1] = result.getData().get("Error");
            return respuesta;
        } else {
            respuesta[0] = "Ok";
            respuesta[1] = result.getData().get("Ok");
            return respuesta;
        }
    }

    public static String[] eliminar(HttpSession session, String sp, String forma, String operacion, String id) {
        String[] respuesta = new String[2];
        String db = session.getAttribute("empresa").toString();
        String usuario = session.getAttribute("usuario").toString();
        Object[] parameters = LoadParameters.run(new Object[]{forma, operacion, usuario, id});
        ResultSingleGenericDao result = JdbcSingleTemplateSP.run(db, sp, parameters);
        if (result.getData().containsKey("Error")) {
            respuesta[1] = result.getData().get("Error");
            respuesta[0] = "Error";
            return respuesta;
        } else {
            respuesta[0] = "Ok";
            respuesta[1] = result.getData().get("Ok");
            return respuesta;
        }
    }


}
