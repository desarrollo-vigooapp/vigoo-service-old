FROM openjdk:8
ADD target/vigoobox.jar vigoobox.jar
EXPOSE 8081
ENTRYPOINT ["java", "-jar", "vigoobox.jar"]